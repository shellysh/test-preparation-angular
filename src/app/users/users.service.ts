import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';

//Fire Base
import { AngularFireDatabase } from 'angularfire2/database';

//Deployment
import { environment } from './../../environments/environment';

@Injectable()
export class UsersService {
  http:Http;

  //Fire Base
  getUsersFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/users').valueChanges();
  }

  //Get
  getUsers(){
    let token = localStorage.getItem('token');
        let options = {
          headers: new Headers({
            'Authorization':'Bearer '+token
          })
    }
    return this.http.get(environment.url + 'users' , options);
  }

  //Post
  postUsers(data){
    let token = localStorage.getItem('token');

    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('email',data.email);
    return this.http.post(environment.url + 'users', params.toString(), options);
  }

  //Delete
  deleteUser(key){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
    return this.http.delete(environment.url + 'users/'+ key, options);
  }

  //Get one user
  getUser(id){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
    return this.http.get(environment.url + 'users/'+ id, options);
  }

  //Put
  putUser(data,key){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('email',data.email);
    return this.http.put(environment.url + 'users/'+ key,params.toString(), options);
  }

  //JWT
  login(credentials){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    //מאפ פועל על המידע שהגיע מהשרת JWT
    return this.http.post(environment.url + 'auth', params.toString(), options).map(response=>{
        let token = response.json().token; //JWT שהגיע מהשרת
        if(token) localStorage.setItem('token' , token);
    });
  }

  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }

}
