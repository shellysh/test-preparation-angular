import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './../users.service';
import {FormGroup , FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;

  updateform = new FormGroup({
    username:new FormControl(),
    email:new FormControl()
  });

  constructor(private route: ActivatedRoute ,service: UsersService) {
    this.service = service;
  }

  //שליחת העדכון
  sendData() {
    this.updateUser.emit(this.updateform.value.username);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateUserPs.emit();
        }
      );
    })
  }

  //הצגת מספר מזהה בשביל הטופס
  user;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}
