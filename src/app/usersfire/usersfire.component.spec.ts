import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersfireComponent } from './usersfire.component';

describe('UsersfireComponent', () => {
  let component: UsersfireComponent;
  let fixture: ComponentFixture<UsersfireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersfireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersfireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
