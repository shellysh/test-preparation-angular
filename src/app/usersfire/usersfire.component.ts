import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';

@Component({
  selector: 'usersfire',
  templateUrl: './usersfire.component.html',
  styleUrls: ['./usersfire.component.css']
})
export class UsersfireComponent implements OnInit {

  users;

  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
        console.log(response);
        this.users = response;
    });
  }

}
