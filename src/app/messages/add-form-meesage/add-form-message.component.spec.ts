import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFormMessageComponent } from './add-form-message.component';

describe('AddFormMessageComponent', () => {
  let component: AddFormMessageComponent;
  let fixture: ComponentFixture<AddFormMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFormMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFormMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
