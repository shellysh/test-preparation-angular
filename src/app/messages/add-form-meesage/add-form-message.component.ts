import { MessagesService } from './../messages.service';
import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { FormGroup , FormControl ,FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'add-form-message',
  templateUrl: './add-form-message.component.html',
  styleUrls: ['./add-form-message.component.css']
})
export class AddFormMessageComponent implements OnInit {
  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>();
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>();

  service:MessagesService;

  addform = new FormGroup({
    message:new FormControl(),
    user_id:new FormControl()
  });

  sendData() {
    //השורה שולחת את התוכן לאופטימיסטיק שנמצא באב
    this.addMessage.emit(this.addform.value.message); //עדכון לאב בהתאם לשדות שהגדרנו למעלה

    this.service.postMessage(this.addform.value).subscribe(
      response => {
        console.log(response.json());
        this.addMessagePs.emit();
      }
    );
  }

  //הגדרת משתנה לשרת ולולידציה
  constructor(service: MessagesService, private formBuilder:FormBuilder) { 
    this.service = service;
  }

  ngOnInit() {
    //הגדרת ולידציה לטופס
    this.addform = this.formBuilder.group({
      message:  [null, [Validators.required]],
      user_id: [null, [Validators.required]],
    });
  }

}
