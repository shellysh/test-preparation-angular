import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';

//Deployment
import { environment } from './../../environments/environment';

@Injectable()
export class MessagesService {
  http:Http;

  //Get
  getMessages(){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
    return this.http.get(environment.url + 'messages', options);
  }

  //Post
  postMessage(data){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token
      })
    }
    //בגרש נגדיר את השדות בהתאם לטופס
    let params = new HttpParams().append('message',data.message).append('user_id',data.user_id);
    return this.http.post(environment.url + 'messages', params.toString(), options);
  }

  constructor(http:Http) {
    this.http = http;
   }

}
