import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service'; //יש לייבא את השרת

//קיצור הסלקטור בהתאמה
@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  //הגדרת המשתנים לשימוש הצגתם
  messages;
  messagesKeys = [];

  //הגדרת ייבוא הנתונים מהשרת ושימוש בפונקציה המתאימה
  constructor(private service:MessagesService) {
    service.getMessages().subscribe(
      response=>{
        this.messages = response.json();
        this.messagesKeys = Object.keys(this.messages);
    })
   }

   //הגישה האופטימית
   optimisticAdd(message){
    //console.log("addMessage work "+message); בדיקה
    var newKey = parseInt(this.messagesKeys[this.messagesKeys.length - 1],0) + 1;
    var newMessageObject = {};
    newMessageObject['body'] = message; //הגדרת השדות של הטבלה
    this.messages[newKey] = newMessageObject;
    this.messagesKeys = Object.keys(this.messages);
  }

  //הגישה הפסימית
  pessimisticAdd(){
    this.service.getMessages().subscribe(
      response=>{
        this.messages = response.json();
        this.messagesKeys = Object.keys(this.messages);
    })
  }

  ngOnInit() {
  }

}
