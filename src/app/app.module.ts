import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'; //הגדרת route
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { UsersService } from './users/users.service';
import { MessagesService } from './messages/messages.service';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { MessagesComponent } from './messages/messages.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddFormComponent } from './users/add-form/add-form.component';
import { AddFormMessageComponent } from './messages/add-form-meesage/add-form-message.component';
import { UserComponent } from './users/user/user.component';
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { LoginComponent } from './login/login.component';

//Fire Base
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { UsersfireComponent } from './usersfire/usersfire.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    MessagesComponent,
    NavigationComponent,
    NotFoundComponent,
    AddFormComponent,
    AddFormMessageComponent,
    UserComponent,
    UpdateFormComponent,
    LoginComponent,
    UsersfireComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
            {path: '', component: UsersComponent},
            {path: 'messages', component: MessagesComponent},
            {path: 'user/:id', component: UserComponent},
            {path: 'update-form/:id', component: UpdateFormComponent},
            {path: 'login', component: LoginComponent},
            {path: 'usersfire', component: UsersfireComponent},
            {path: '**', component: NotFoundComponent}
          ])
  ],
  providers: [
    UsersService,
    MessagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
